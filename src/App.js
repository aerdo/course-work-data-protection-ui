import './App.css';
import React, {useState, useEffect} from "react";
import Routing from "./Pages/Routing";
import link from "./Components/CryptoHelper";


async function checkConnection(){
    try {
        const response = await fetch(`${link}/api/check`);
        return await response.json();
    }catch{
        return("Error");
    }
}

function App() {
    const [check, setCheck] = useState();
    useEffect(()=>{
        if (!check) {
            checkConnection().then((res)=>{
                if (res.status === "OK") setCheck(true);
            });
        }
    })
    if (check){
        return (
            <Routing/>
        );
    }else{
        return(<></>);
    }

}

export default App;
