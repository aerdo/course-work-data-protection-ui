import React from "react";
import {Button, Container, Nav, Navbar} from "react-bootstrap";
import {useHistory} from "react-router-dom";
import {useAuth} from "../../Pages/Routing";
import {useSelector} from "react-redux";

function NavBar(){
    const history = useHistory();
    const auth = useAuth();
    const logOut = () => {
        auth.signOut(() => {
            //window.location.reload();
            history.push("/auth");
        });
    };


    let user = useSelector(state=>state.user);

    return(
        <div style={ user ? {} : {visibility: "hidden"} }>
            <Navbar bg="dark" variant="dark" style={{marginRight: "calc(-1 * (100vw - 100%))"}}>
                <Container>
                    <Nav className="me-auto" activeKey={history.location.pathname}>
                        <Nav.Link href="/profile">Профиль</Nav.Link>
                        <Nav.Link style={user && user.user_id === 1 ? {} : {visibility: "hidden"}}
                                  href="/administration" >
                            Администрирование
                        </Nav.Link>
                    </Nav>
                    <Nav>
                        <Nav.Item>
                            <Button variant="outline-light" onClick={logOut}>Выйти</Button>
                        </Nav.Item>
                    </Nav>
                </Container>
            </Navbar>
        </div>
    )
}

export default NavBar;