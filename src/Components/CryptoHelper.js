import crypto from 'crypto';

//const link = "http://localhost:3001";
const link = "https://data-protection-coursework-api.herokuapp.com";
export default link;

export function genHash(answer) {
    return crypto.createHash('sha512').update(answer).digest('hex');
}
