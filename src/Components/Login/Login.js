import React, {useState} from "react";
import {Button, Card, Form, Spinner} from "react-bootstrap";

import {useHistory} from "react-router-dom";
import {useAuth} from "../../Pages/Routing";
import {useDispatch} from "react-redux";
import { userLoggedIn } from "../Store/Actions";
import {genHash} from "../CryptoHelper";

import link from "../CryptoHelper";

async function getName(userName){
    try {
        const response = await fetch(`${link}/api/login/${userName}`);
        return await response.json();
    }catch{
        console.error("Error!");
    }
}

async function getQuestion(userId, questionNum){
    try {
        const response = await fetch(`${link}/api/login/get_question/${userId}/${questionNum}`);
        return await response.json();
    }catch{
        console.error("Error!");
    }
}

async function getCheck(questionId, data){
    try {
        const response = await fetch(`${link}/api/login/check_answer/${questionId}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        });
        return await response.json();
    }catch{
        console.error("Error!");
    }
}

function Login(){
    document.title = "Аутентификация";
    const timeout = 200;

    const dispatch = useDispatch();

    const history = useHistory();
    const auth = useAuth();

    let logIn = () => {
        const user = {
            user_id: userId,
            user_name: name
        }
        auth.signIn(() => {
            dispatch(userLoggedIn(user));
            history.replace("/profile");
        });
    };

    const [login, setLogin] = useState("1");
    const [isLoading, setLoading] = useState(false);
    const [message, setMessage] = useState("");
    const [hide, setHide] = useState(true);

    // имя
    const [name, setName] = useState("");
    const [correctName, setCorrectName] = useState(true);

    // вопрос и ответ
    const [userId, setUserId] = useState("");
    const [questionId, setQuestionId] = useState("");
    const [question, setQuestion] = useState("");
    const [answer, setAnswer] = useState("");
    const [correctAnswer, setCorrectAnswer] = useState(true);
    const [lTry, setLTry] = useState(0);

    const handleLogin = (value) => {
        setLoading(false);
        setLogin(value);
        if (value === "3"){
            setTimeout(()=>logIn(), timeout*4);
        }
    }

    const checkName = () => {
        if (name === '') {
            setLoading(false);
            setCorrectName(false);
            setMessage("Поле не должно быть пустым")
        } else {
            getName(name).then(res=>{
                if (res.message) {
                    setLoading(false);
                    setCorrectName(false);
                    setMessage("Неверное имя пользователя. Вы прошли регистрацию?");
                } else {
                    setCorrectName(true);
                    setMessage("");
                    const id = res.user_id;
                    const questionNum = res.question_num;

                    setUserId(res.user_id);
                    setLTry(res.attempts_amount);

                    getQuestion(id,questionNum).then((res)=>{
                        if (res.message){
                        }else{
                            const first = res.question_text === '-';
                            if (first){
                                const user = {
                                    user_id: id,
                                    user_name: name
                                }
                                localStorage.setItem('first', JSON.stringify(user));
                                history.replace("/firstLogin");
                            }else{
                                setQuestion(res.question_text);
                                setQuestionId(res.question_id);
                                setLoading(false);
                                handleLogin("2");
                            }

                        }
                    }).catch(err=>{

                    });
                }
            }).catch(err=>{

            })
        }
    }

    const checkAnswer = () => {

        if (answer.length === 0){
            setLoading(false);
            setCorrectAnswer(false);
            setMessage("Поле не должно быть пустым");
        } else {
            const answerHash = genHash(answer);
            const data = {
                answer: question === "?" ? answer : answerHash
            }

            getCheck(questionId, data).then(res=>{
                setLoading(false);
                if (!res.access){
                    if (res.message === "Try again"){
                        setCorrectAnswer(false);
                        if (lTry !== 1){
                            setMessage("Неверный ответ. Осталось попыток: "+(lTry-1))
                            setLTry(lTry-1);
                        }else {
                            setLogin("4");
                        }
                    }else{

                    }
                }else{
                    setCorrectAnswer(true);
                    setMessage("");
                    handleLogin("3");
                }
            }).catch(err=>{

            })
        }

    }

    // переключатель
    const handleProcess = (value) => {
        setLoading(true);
        if (value === "2"){
            setTimeout(()=>checkName(),timeout);
        } else if (value === "3") {
            setTimeout(()=>checkAnswer(),timeout*2);
        } else {

        }
    }

    // 1 - аутентификация начата, пытаемся ввести имя
    // 2 - ввели верно имя, пытаемся ввести пароль
    // 3 - ввели пароль, завершаем аутентификацию

    if (login === "1"){
        return(
            <Card body>
                <Form>
                    <Form.Group className="mb-3">
                        <Form.Label>Имя пользователя:</Form.Label>
                        <Form.Control type="text" placeholder="Имя пользователя" disabled={isLoading}
                                      onKeyPress={(e)=> {
                                          if (e.key === "Enter") {
                                              e.preventDefault();
                                              handleProcess("2");
                                          }
                                      }}
                                      value={name}
                                      onChange={(e)=>{
                                          setName(e.target.value);
                                          setMessage("");
                                      }}/>
                        <Form.Text>
                            {correctName ? " " : message}
                        </Form.Text>
                    </Form.Group>
                    {isLoading ?
                        (
                            <Button variant="dark">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                <span className="visually-hidden">Loading...</span>
                            </Button>
                        ) :
                        (
                            <Button variant="dark" onClick={()=>handleProcess("2")} disabled={isLoading}>
                        {'Продолжить'}
                            </Button>
                        )
                    }
                </Form>
           </Card>
        )
    } else if (login === "2")
    {
        return(
            <Card body>
                <Form>
                    <Form.Group className="mb-3">
                        <Form.Label>Вопрос:</Form.Label>
                        <p style={{fontWeight: "bold"}}>{question}</p>
                        <Form.Control type={hide ? "password" : "text"} autoComplete="off" placeholder="Ответ" disabled={isLoading}
                                      onKeyPress={(e)=> {
                                          if (e.key === "Enter") {
                                              e.preventDefault();
                                              handleProcess("3");
                                          }
                                      }}
                                      value={answer}
                                      onChange={(e)=>{
                                          setAnswer(e.target.value);
                                          setMessage("");
                                      }}/>
                        <Form.Check
                            type='checkbox'
                            label={"Показывать ввод"}
                            onClick={()=>setHide(!hide)}
                        />
                        <Form.Text>
                            {correctAnswer ? " " : message}
                        </Form.Text>

                    </Form.Group>
                    {isLoading ?
                        (
                            <Button variant="dark">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                <span className="visually-hidden">Loading...</span>
                            </Button>
                        ) :
                        (
                            <Button variant="dark" onClick={()=>handleProcess("3")} disabled={isLoading}>
                                {'Продолжить'}
                            </Button>
                        )
                    }
                </Form>
            </Card>
        )
    } else if (login === "3") {
        return (
            <Card body>
                <p>Успешно! Вход в систему...</p>
                <Spinner animation="border" />
            </Card>
        )
    } else {
        return (
            <Card body>
                <h5>Ошибка :( </h5>
                <p>Попробуйте позже</p>
            </Card>
        )
    }

}

export default Login;