import React, {useState} from "react";
import {Button, Card, Spinner} from "react-bootstrap";

import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";

import {useAuth} from "../../Pages/Routing";
import {userLoggedIn} from "../Store/Actions";
import SingleQuestion from "./SingleQuestion";

import link from "../CryptoHelper";

async function getQuestionNums(userId){
    try {
        const response = await fetch(`${link}/api/users/${userId}/questions`);
        return await response.json();
    }catch{
        console.error("Error");
    }
}

async function putQuestions(questId, data){
    try {
        const response = await fetch(`${link}/api/questions/${questId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        //console.error("Error!!!");
    }
}

function QuestionsInput(props){
    const [isLoading, setLoading] = useState(false);
    const [currentNum, setCurrentNum] = useState(0);
    const [ready, setReady] = useState(false);
    const [questions, setQuestions] = useState([]);

    const dispatch = useDispatch();
    const history = useHistory();
    const auth = useAuth();

    const [nums, setNums] = useState([]);

    useState(()=>{
        if (nums.length === 0) {
            getQuestionNums(props.user.user_id).then(res=>{
                setNums(res);
            });
        }
    })

    const addQuestion = (id, data) => {
        setQuestions([...questions, data]);
    }

    const nextQuestion = (num) => {
        if (num!==nums.length-1) {
            setCurrentNum(num+1)
        }
        else if (num === nums.length-1){
            setReady(true);
        }
    }

    const handleSaveQuestions = () => {
        auth.signIn(() => {
            dispatch(userLoggedIn(props.user));
            history.replace("/profile");
        })
    };

    const handleBeginLoading = () => {
        setLoading(true);
        nums.forEach((item,id)=>{
            putQuestions(item.question_id, questions[id]).then();
        })
        setTimeout(handleSaveQuestions, 300);
    }

    return(
        <div style={{marginBottom: "80px"}}>
            <Card body style={{width: "60%"}}>
                {!ready ?
                    (<SingleQuestion id = {currentNum} addQuestions = {addQuestion} nextQuestion = {nextQuestion} userId = {props.user.user_id}/>):
                    (
                        <>
                            <h4>Замечание</h4>
                            <p>Если при последующих входах Вам будет предложен вопрос следующего содержания: "?", то это значит, что администратор увеличил количество вопросов для Вашего аккаунта</p>
                            <p>Ответ на этот вопрос: "?"</p>
                            <p>Пожалуйста, если Вы обнаружите такие изменения, зайдите в настройки профиля и измените вопросы, состоящие только из "?"</p>
                            <p>Также обращаем внимание, что количество вопросов для Вас также может быть уменьшено аадминистратором</p>
                            {!isLoading ? (
                                <Button form="firstLogin" variant="dark"
                                        onClick={handleBeginLoading}> Понятно </Button>
                            ) : (<Spinner animation="border" />)}
                        </>
                    )
                }

            </Card>
        </div>
    )
}

export default QuestionsInput;