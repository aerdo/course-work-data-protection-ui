import React, {useState, useEffect} from "react";
import {Button, Form} from "react-bootstrap";
import {genHash} from "../CryptoHelper";

function SingleQuestion(props) {
    const [question, setQuestion] = useState("");
    const [answer, setAnswer] = useState("");
    const [prevQuestions, setPrevQuestions] = useState([]);

    const [messageQuestion, setMessageQuestion] = useState("");
    const [messageAnswer, setMessageAnswer] = useState("");
    const [hide, setHide] = useState(true);

    useEffect(()=>{
        setQuestion("");
        setAnswer("");
        setMessageQuestion("");
        setMessageAnswer("");
    }, [props.id]);

    const getNextQuestion = () => {
        if (!question) {
            setMessageQuestion("Поле не должно быть пустым");
        } else {
            setMessageQuestion("");
        }
        if (!answer) {
            setMessageAnswer("Поле не должно быть пустым");
        } else {
            setMessageAnswer("");
        }
        if (question && answer){
            addQuestion();
        }
    }

    const addQuestion = () => {

        if (question==='-' || question==='?') {
            setMessageQuestion("Введите другой вопрос");
        }else{
            if (props.id !== 0) {
                if ( !prevQuestions.includes(question)) {
                    const answerHash = genHash(answer);
                    const data = {
                        user_id: props.userId,
                        question_text: question,
                        answer_text: answerHash
                    }

                    setPrevQuestions([...prevQuestions, question]);
                    props.addQuestions(props.id, data);
                    props.nextQuestion(props.id);
                }else{
                    setMessageQuestion("Введите другой вопрос");
                }
            } else{
                const answerHash = genHash(answer);
                const data = {
                    user_id: props.userId,
                    question_text: question,
                    answer_text: answerHash
                }

                setPrevQuestions([...prevQuestions, question]);
                props.addQuestions(props.id, data);
                props.nextQuestion(props.id);
            }
        }
    }

    return(
        <div>
            <p>Вопрос № {props.id+1}</p>
            <Form id="firstLogin" >
                <Form.Group style={{marginBottom: "20px"}}>
                    <Form.Control type="text" placeholder="Вопрос" value={question}
                                  onChange={(e)=>{
                                      setQuestion(e.target.value);
                                      setMessageQuestion("");
                                  }}
                    />
                    <Form.Text>
                        {messageQuestion}
                    </Form.Text>
                </Form.Group>
                <Form.Group style={{marginBottom: "20px"}}>
                    <Form.Control type={hide ? "password" : "text"} placeholder="Ответ" value={answer}
                                  onChange={(e)=>{
                                      setAnswer(e.target.value)
                                      setMessageAnswer("");
                                  }}
                    />
                    <Form.Check
                        type='checkbox'
                        label={"Показывать ввод"}
                        onClick={()=>setHide(!hide)}
                    />
                    <Form.Text>
                        {messageAnswer}
                    </Form.Text>
                </Form.Group>
            </Form>
            <Button form="firstLogin" variant="dark"
                    onClick={getNextQuestion}> Дальше </Button>
        </div>
    )
}

export default SingleQuestion;