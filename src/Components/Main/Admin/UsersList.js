import React, {useState, useEffect} from "react";
import {Table} from "react-bootstrap";
import AddUser from "./AddUser/AddUser";
import UserReview from "./UserReview/UserReview";
import link from "../../CryptoHelper";

function UsersList(){
    const [users, setUsers] = useState([]);

    async function fetchUsers() {
        try {
            const response = await fetch(`${link}/api/users`)
            const json = await response.json();
            setUsers(json);
        }catch{
            console.error("Error getting users");
        }
    }
    useEffect(() => {
        if (users.length === 0) fetchUsers().then();
    });

    return(
        <>
            <h4>Список пользователей</h4>
            <AddUser />
            <Table bordered hover style={{width: "60%"}}>
                <thead >
                <tr>
                    <th style={{width: "8%"}}>№</th>
                    <th >Имя пользователя</th>
                </tr>
                </thead>
                <tbody>
                {users.map((item,id)=>{
                    return(
                        <UserReview key={id} item = {item} id = {id}/>
                    )
                })}
                </tbody>
            </Table>
        </>
    )
}

export default UsersList;