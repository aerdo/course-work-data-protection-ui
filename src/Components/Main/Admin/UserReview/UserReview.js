import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import link from "../../../CryptoHelper";

async function deleteUser(userId){
    try {
        const response = await fetch(`${link}/api/users/${userId}`,{
            method: 'DELETE',
        });
        return await response.json();
    }catch{
        console.error("Error!");
    }
}

async function updateUser(userId, data){
    try {
        const response = await fetch(`${link}/api/users/${userId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        console.error("Error!");
    }
}

function UserReview(props){
    const user = props.item;
    const [change, setChange] = useState(false);

    const [questions, setQuestions] = useState(user.questions_amount);
    const [attempts, setAttempts] = useState(user.attempts_amount);

    const [messageQuestions, setMessageQuestions] = useState("");
    const [messageAttempts, setMessageAttempts] = useState("");

    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
        setChange(false);
        setMessageQuestions("");
        setMessageAttempts("");
    }
    const handleShow = () => {
        setShow(true);
    }

    const handleDelete = () =>{
        deleteUser(user.user_id).then(res=>{
            if (res && res.message){

            }else{
                handleClose();
                window.location.reload();
            }
        })
    }

    const handleBack = () => {
        setChange(false);
        setQuestions(user.questions_amount);
        setAttempts(user.attempts_amount);
        setMessageQuestions("");
        setMessageAttempts("");
    }

    const handleSubmit = () => {
        let fl1, fl2;
        if (!questions) {
            setMessageQuestions("Поле не должно быть пустым");
        } else {
            //setMessageQuestions("");
            let num1 = Number(questions);
            if (!Number.isInteger(num1)){
                setMessageQuestions("Введите целое число");
                fl1 = false;
            } else {
                fl1 = true;
                setMessageQuestions("");
            }
        }
        if (!attempts ) {
            setMessageAttempts("Поле не должно быть пустым");
        } else {
            //setMessageAttempts("");
            const num2 = Number(attempts);
            if (!Number.isInteger(num2)) {
                setMessageAttempts("Введите целое число");
                fl2 = false;
            } else {
                fl2 = true;
                setMessageAttempts("");
            }
        }
        if (questions && attempts && fl1 && fl2){
            const data = {
                questions_amount: questions.toString(),
                attempts_amount: attempts.toString()
            }
            updateUser(user.user_id,data).then(res=>{
                if (res.message){
                    //console.log(JSON.stringify(res));
                } else {
                    setChange(false);
                    handleClose();
                    window.location.reload();
                }
            })

        }
    }

    return(
        <>
            <tr onClick={handleShow}>
                <td>{props.id+1}</td>
                <td>{user.user_name}</td>
            </tr>
            <Modal
                show={show}
                onHide={handleClose}
                /*backdrop="static"*/
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Просмотр пользователя</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <div style={{marginBottom: "15px"}}>
                        <div style={{display: "inline-block", marginRight: "5px"}}>Имя пользователя:</div>
                        <div style={{fontWeight: "bold", display: "inline-block"}}>{ user.user_name } </div>
                    </div>
                    <p>
                        Количество вопросов: { !change ?
                        (user.questions_amount) :
                        (<>
                            <Form.Control type="text" placeholder="Количество вопросов" style={{marginBottom: "0"}} value={questions}
                                          onChange={(e)=>{
                                              setQuestions(e.target.value);
                                              setMessageQuestions("");
                                          }}/>
                            <Form.Text style={{marginTop: "0"}}> {messageQuestions} </Form.Text>
                        </>)}
                    </p>

                    <p>
                        Количество попыток: {!change ?
                        (user.attempts_amount) :
                        (<>
                            <Form.Control type="text" placeholder="Количество попыток" value={attempts} style={{marginBottom: "0"}}
                                          onChange={(e)=>{
                                              setAttempts(e.target.value);
                                              setMessageAttempts("");
                                          }}/>
                            <Form.Text style={{marginTop: "0"}}> {messageAttempts} </Form.Text>
                        </>)}
                    </p>
                    <p style={change && (user.user_id !== 1 ) ? {cursor: "pointer", fontSize: "13px"} : {visibility: "hidden"}}
                        onClick={handleDelete}>Удалить пользователя?</p>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>

                    {change ?
                        (<>
                            <Button form="addUser" variant="outline-dark" onClick={handleBack}>Назад к просмотру</Button>
                            <Button form="addUser" variant="dark" onClick={()=>setTimeout(handleSubmit, 150)}>Сохранить</Button>
                        </> ) :
                        (<>
                            <Button form="addUser" variant="outline-dark" onClick={handleClose}>Закрыть</Button>
                            <Button form="addUser" variant="dark" onClick={()=>setChange(true)}>Изменить</Button>
                        </> )}

                </Modal.Footer>
            </Modal>
        </>
    )
}

export default UserReview;