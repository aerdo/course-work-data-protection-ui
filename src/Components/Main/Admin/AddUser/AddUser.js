import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import link from "../../../CryptoHelper";

async function postData(data){
    try {
        const response = await fetch(`${link}/api/users/`,{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return await response.json();
    }catch{
        console.error("Error");
    }
}

function AddUser(){
    const [userName, setUserName] = useState("");
    const [questions, setQuestions] = useState("");
    const [attempts, setAttempts] = useState("");

    const [messageName, setMessageName] = useState("");
    const [messageQuestions, setMessageQuestions] = useState("");
    const [messageAttempts, setMessageAttempts] = useState("");

    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);

        setUserName("");
        setQuestions("");
        setAttempts("");

        setMessageName("");
        setMessageQuestions("");
        setMessageAttempts("");

    };
    const handleShow = () => {
        setShow(true);
    }

    const handleCheck = () => {
        let fl1, fl2;

        if (! userName ) {
            setMessageName("Поле не должно быть пустым");
        } else {
            setMessageName("");
        }
        if (! questions ) {
            setMessageQuestions("Поле не должно быть пустым");
        } else {
            //setMessageQuestions("");
            let num1 = Number(questions);
            if (!Number.isInteger(num1)){
                setMessageQuestions("Введите целое число");
                fl1 = false;
            } else {
                fl1 = true;
                setMessageQuestions("");
            }
        }
        if (! attempts ) {
            setMessageAttempts("Поле не должно быть пустым");
        } else {
            //setMessageAttempts("");
            const num2 = Number(attempts);
            if (!Number.isInteger(num2)) {
                setMessageAttempts("Введите целое число");
                fl2 = false;
            } else {
                fl2 = true;
                setMessageAttempts("");
            }
        }

        if (userName && questions && attempts && fl1 && fl2){
            setMessageName("");
            setMessageQuestions("");
            setMessageAttempts("");
            const data = {
                user_name: userName,
                questions_amount: questions,
                attempts_amount: attempts
            }
            postData(data).then((res)=>{
                if (res && res.message) {
                    if (res.message === 'Username has to be unique'){
                        setMessageName("Имя занято. Попробуйте другое");
                    }
                } else if (res){
                    setMessageName("");
                    setMessageQuestions("");
                    setMessageAttempts("");
                    //console.log("Success: ",JSON.stringify(res));
                    handleClose();
                    window.location.reload();
                }
            });
        }
    }

    return(
        <>
            <Button variant="outline-dark" style={{margin: "10px 0 20px 0"}} onClick={handleShow}>Новый пользователь</Button>
            <Modal
                show={show}
                onHide={handleClose}
                /*backdrop="static"*/
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Новый пользователь</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="addUser" >
                        <Form.Group style={{marginBottom: "20px"}}>
                            <Form.Control type="text" placeholder="Имя пользователя" value={userName}
                                          onChange={(e)=>{
                                              setUserName(e.target.value);
                                              setMessageName("");
                                          }}/>
                            <Form.Text>
                                {messageName}
                            </Form.Text>
                        </Form.Group>
                        <Form.Group style={{marginBottom: "20px"}}>
                            <Form.Control type="text" placeholder="Количество вопросов" value={questions}
                                          onChange={(e)=>{
                                              setQuestions(e.target.value);
                                              setMessageQuestions("");
                                          }}/>
                            <Form.Text>
                                {messageQuestions}
                            </Form.Text>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Количество попыток" value={attempts}
                                          onChange={(e)=>{
                                              setAttempts(e.target.value);
                                              setMessageAttempts("");
                                          }}/>
                            <Form.Text>
                                {messageAttempts}
                            </Form.Text>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addUser" variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button form="addUser" variant="dark" onClick={()=>setTimeout(handleCheck, 100)}>Добавить</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddUser;