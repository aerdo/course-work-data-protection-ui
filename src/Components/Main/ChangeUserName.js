import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {userChangedName} from "../Store/Actions";

import link from "../CryptoHelper";

async function updateUser(userId, data){
    try {
        const response = await fetch(`${link}/api/users/rename/${userId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        console.error("Error!");
    }
}

function ChangeUserName(props){
    const dispatch = useDispatch();

    const user = props.user;
    const [show, setShow] = useState(false);
    const [name, setName] = useState(props.user.user_name);
    const [message, setMessage] = useState("");

    const handleClose = () => {
        setShow(false);
        setName(props.user.user_name);
        setMessage("");
    }
    const handleShow = () => {
        setShow(true);
    }

    const handleSave = () => {
        if (!name){
            setMessage("Поле не должно быть пустым")
        }else{
            if (name === user.user_name){
                setMessage("");
                handleClose();
            }else{
                setMessage("");
                const data = {
                    user_name: name
                }
                user.user_name = name;
                updateUser(user.user_id, data).then(res=>{
                    if (res.message) {
                        if (res.message === 'Username has to be unique'){
                            setMessage("Имя занято. Попробуйте другое");
                        }
                    } else {
                        setMessage("");
                        dispatch(userChangedName(user));
                        handleClose();

                        window.location.reload();
                    }
                })
            }

        }
    }

    return(
        <div>
            <Button variant="outline-dark" onClick={handleShow}>Изменить имя пользователя</Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Изменить имя пользователя</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form.Control type="text" placeholder="Имя пользователя" value={name} style={{marginBottom: "0"}}
                                  onChange={(e)=>{
                                      setName(e.target.value);
                                      setMessage("");
                                  }}
                                  onKeyPress={(e)=> {
                                      if (e.key === "Enter") {
                                          e.preventDefault();
                                          handleSave();
                                      }
                                  }}/>
                    <Form.Text style={{marginTop: "0"}}> {message} </Form.Text>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                        <Button form="addUser" variant="outline-dark" onClick={handleClose}>Отмена</Button>
                        <Button form="addUser" variant="dark" onClick={handleSave}>Сохранить</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default ChangeUserName;