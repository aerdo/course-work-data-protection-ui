import React, {useState} from "react";
import {Button, Form, Modal, Card} from "react-bootstrap";
import {genHash} from "../CryptoHelper";
import link from "../CryptoHelper";

async function getQuestions(userId){
    try {
        const response = await fetch(`${link}/api/users/${userId}/questions/full`);
        return await response.json();
    }catch{
        console.error("Error");
    }
}

async function putQuestion(questId, data){
    try {
        const response = await fetch(`${link}/api/questions/${questId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await response.json();
    }catch{
        //console.error("Error!!!");
    }
}

function ChangeQuestions(props) {
    const user = props.user;
    const [show, setShow] = useState(false);
    const [questions, setQuestions] = useState([]);

    const [question, setQuestion] = useState("");
    const [answer, setAnswer] = useState("");
    const [hide, setHide] = useState(true);

    const [messageQuestion, setMessageQuestion] = useState("");
    const [messageAnswer, setMessageAnswer] = useState("");

    const [cardId, setId] = useState(-1);
    const [check, setCheck] = useState(true);

    const handleClose = () => {
        setShow(false);

        setId(-1);
        setCheck(true);
        setHide(true);

        setQuestion("");
        setAnswer("");
    }

    const handleShow = () => {
        if (questions.length === 0) {
            getQuestions(user.user_id).then(res=>{
                setQuestions(res);
            })
        }
        setShow(true);
    }

    const handleEdit = (id) => {
        if (check){
            setId(id);
            setHide(true);
            setCheck(false);
            setQuestion(questions[id].question_text);
        }
    }
    const handleCancel = () => {
        setId(-1);
        setCheck(true);
        setHide(true);
        setQuestion("");
        setAnswer("");
        setMessageQuestion("");
        setMessageAnswer("");
    }

    const handleSave = () => {
        if (!question) {
            setMessageQuestion("Поле не должно быть пустым");
        } else {
            setMessageQuestion("");
        }
        if (!answer) {
            setMessageAnswer("Поле не должно быть пустым");
        } else {
            setMessageAnswer("");
        }
        if (question && answer){
            if (question==='-' || question==='?') {
                setMessageQuestion("Введите другой вопрос");
            } else {
                if (question === questions[cardId].question_text){
                    // если вопрос прежний, хотим перезаписать ответ
                    const answerHash = genHash(answer);
                    const data = {
                        question_text: question,
                        answer_text: answerHash
                    }

                    putQuestion(questions[cardId].question_id, data).then(()=>{
                        questions[cardId].question_text = question;
                        setId(-1);
                        setCheck(true);
                        setHide(true);
                        setQuestion("");
                        setAnswer("");
                    })


                } else {
                    // если вопрос другой, проверяем на повторение
                    const pos = questions.findIndex(item=>item.question_text === question);
                    if (pos === -1){
                        const answerHash = genHash(answer);
                        const data = {
                            question_text: question,
                            answer_text: answerHash
                        }

                        putQuestion(questions[cardId].question_id, data).then(()=>{
                            questions[cardId].question_text = question;
                            setId(-1);
                            setCheck(true);
                            setHide(true);
                            setQuestion("");
                            setAnswer("");
                        })

                    } else {
                        setMessageQuestion("Введите другой вопрос")
                    }
                }
            }
        }
    }


    return(
        <div style={{marginTop: "15px", marginBottom: "30px"}}>
            <Button variant="outline-dark" onClick={handleShow}>Просмотр вопросов</Button>
            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem", paddingBottom: "0"}}>
                    <h4>Просмотр вопросов</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Button variant="outline-dark" onClick={handleClose} style={{marginBottom: "10px"}}>
                        <i className="fas fa-arrow-left"/> Назад
                    </Button>
                    {questions.map((item, id)=>{
                        return(
                            <div key={id} style={{marginBottom: "20px"}}>
                                <Card body bg={ cardId === id ? "" : "light"} style={cardId !== id ? {height: "110px"} : {}}>
                                    <p>Вопрос № {id+1}</p>
                                    <Form id={"question"+id}>
                                        <div>
                                            <Form.Group
                                                style={{display: "inline-block",
                                                    verticalAlign: "top",
                                                    marginRight:"10px",
                                                    width: "315px"}}
                                            >
                                                <Form.Control type="text" placeholder={cardId !== id ? item.question_text : "Вопрос"}
                                                    value={cardId === id ? question : item.question_text}
                                                              readOnly={cardId !== id}
                                                    onChange={(e)=>{
                                                        setQuestion(e.target.value);
                                                        setMessageQuestion("");
                                                    }}
                                                />
                                                <Form.Text>
                                                    {cardId === id ? (messageQuestion) : (<></>)}
                                                </Form.Text>
                                            </Form.Group>

                                            <div style={{display: "inline-block", verticalAlign: "middle"}}>
                                                {
                                                    cardId !== id ?
                                                        (<Button variant="outline-dark" disabled={!check}
                                                                 title="Редактировать"
                                                                 onClick={()=>handleEdit(id)}>
                                                            <i className="fas fa-pen"/>
                                                        </Button>) :
                                                        (<Button variant="dark"
                                                                 title="Сохранить изменения"
                                                                 onClick={handleSave}>
                                                            <i className="fas fa-check"/>
                                                        </Button>)
                                                }
                                            </div>
                                        </div>

                                        <div style = {cardId === id ? {marginTop: "10px"} : {visibility: "hidden"}}>
                                            <Form.Group style={{display: "inline-block",
                                                verticalAlign: "top",
                                                marginRight:"10px",
                                                width: "315px"}}
                                            >
                                                <Form.Control type={hide ? "password" : "text"}
                                                              placeholder="Ответ" value={answer}
                                                              readOnly={cardId !== id}
                                                              onChange={(e)=>{
                                                                  setAnswer(e.target.value);
                                                                  setMessageAnswer("");
                                                              }}
                                                />
                                                <Form.Text>
                                                    {messageAnswer}
                                                </Form.Text>
                                                <Form.Check
                                                    type='checkbox'
                                                    label={"Показывать ввод"}
                                                    onClick={()=>setHide(!hide)}
                                                />
                                            </Form.Group>
                                            <Button variant="outline-dark"
                                                    title="Отмена"
                                                    onClick={handleCancel}
                                                    style={{display: "inline-block", width: "42px", marginTop: "0"}}>
                                                <i className="fas fa-times"/>
                                            </Button>
                                        </div>

                                    </Form>
                                </Card>
                            </div>
                        )
                    })}
                </Modal.Body>

            </Modal>
        </div>
    )
}

export default ChangeQuestions;