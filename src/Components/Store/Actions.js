export const userLoggedIn = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
    return{
        type: 'USER_LOGGED_IN',
        user
    };
}

export const userChangedName = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
    return{
        type: 'USER_LOGGED_IN',
        user
    };
}

export const userLoggedOut = () => {
    localStorage.removeItem('user');
    return{
        type: 'USER_LOGGED_OUT'
    };
}