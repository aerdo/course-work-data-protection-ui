const initialState={
    user: JSON.parse(localStorage.getItem('user'))
}

export const rootReducer = (state= initialState, action) => {
    switch (action.type) {
        case "USER_LOGGED_IN":
            return {...state, user: action.user};
        case "USER_LOGGED_OUT":
            return {...state, user: null};
        default:
            return state;
    }
}