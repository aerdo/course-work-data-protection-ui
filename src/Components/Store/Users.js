export const USERS = [
    {
        id: 1,
        name: "nick",
        isAdmin: true
    },
    {
        id: 2,
        name: "rick",
        isAdmin: false
    }
]