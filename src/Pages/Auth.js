import React from "react";
import Login from "../Components/Login/Login";

function Auth(){
    return(
            <div style={styles.loginContainer}>
                <h2 style={{margin: "0 0 20px 15px"}}>Аутентификация</h2>
                <Login/>
            </div>
        )
}

const styles = {
    loginContainer: {
        height: "160px",
        width: "500px",

        position: "absolute",
        top: "36%",
        left: "50%",
        transform: "translate(-50%,-50%)",
    }
}

export default Auth;