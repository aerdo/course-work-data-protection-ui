import React, { useContext, createContext } from "react";

import Profile from "./Profile";
import Auth from "./Auth";
import NavBar from "../Components/NavBar/NavBar";
import Administration from "./Administration";
import FirstLogin from "./FirstLogin";

import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import { userLoggedOut } from "../Components/Store/Actions";
import { useSelector, useDispatch } from "react-redux";

const myAuth = {
    isAuthenticated: false,
    signIn(cb) {
        myAuth.isAuthenticated = true;
        setTimeout(cb, 100);
    },
    signOut(cb) {
        myAuth.isAuthenticated = false;
        setTimeout(cb, 200);
    }
};

const authContext = createContext();

function ProvideAuth({ children }) {
    const auth = useProvideAuth();
    return (
        <authContext.Provider value={auth}>
            {children}
        </authContext.Provider>
    );
}

export function useAuth() {
    return useContext(authContext);
}

function useProvideAuth() {
    const dispatch = useDispatch();

    /*const handleLoggedIn=()=>{
        dispatch(userLoggedIn())
    }*/

    const handleLoggedOut=()=>{
        dispatch(userLoggedOut())
    }

    const signIn = cb => {
        return myAuth.signIn(() => {
            //handleLoggedIn();
            cb();
        });
    };

    const signOut = cb => {
        return myAuth.signOut(() => {
            handleLoggedOut();
            cb();
        });
    };

    return {
        signIn,
        signOut
    };
}

function PrivateRoute({ children, ...rest }) {
    const user = useSelector(state=>state.user);
    return (
        <Route
            {...rest}
            render={({ location }) =>
                user ? (children)
                    :
                    (<Redirect
                        to={{
                            pathname: "/auth",
                            state: { from: location }
                        }}
                    />)

            }
        />
    );
}

function RedirectFromAdmin({children, ...rest}) {
    const user = useSelector(state=>state.user);
    const isAdmin = user.user_id === 1;
    return (
        <Route
            {...rest}
            render={() =>
                isAdmin ? ( <Administration/> ) :
                    (<Redirect
                        to={{
                            pathname: "/profile"
                        }}
                    />)
            }
        />
    );
}

function RedirectFromAuth({ children, ...rest }) {
    const user = useSelector(state=>state.user);
    return (
        <Route
            {...rest}
            render={({ location }) =>
                !user ? ( children ) :
                    (<Redirect
                        to={{
                            pathname: "/profile",
                            state: { from: location }
                        }}
                    />)
            }
        />
    );
}

function FirstLoginRoute({ children, ...rest }) {
    const user = JSON.parse(localStorage.getItem('first'));
    localStorage.removeItem('first');
    return (
        <Route
            {...rest}
            render={() =>
                !user ? (<Redirect
                        to={{
                            pathname: "/auth"
                        }}
                    />)
                    :
                    ( <FirstLogin exact path="/firstLogin" user = {user}/> )
            }
        />
    );
}

function Routing(){
    return(
        <ProvideAuth>
            <Router>
                <NavBar/>
                <Switch>
                    <PrivateRoute exact path="/" >
                        <Redirect
                            to={{
                                pathname: "/profile",
                            }}
                        />
                    </PrivateRoute>

                    <PrivateRoute exact path="/profile">
                        <Profile/>
                    </PrivateRoute>

                    <PrivateRoute exact path="/administration">
                        <RedirectFromAdmin />
                    </PrivateRoute>

                    <RedirectFromAuth exact path="/auth">
                        <Auth/>
                    </RedirectFromAuth>

                    <FirstLoginRoute/>
                </Switch>
            </Router>
        </ProvideAuth>

    )

}
export default Routing;