import React from "react";
import {useSelector} from "react-redux";

import ChangeUserName from "../Components/Main/ChangeUserName";
import ChangeQuestions from "../Components/Main/ChangeQuestions";

function Profile(){
    document.title = "Профиль";

    let user = useSelector(state=>state.user);

    return(
        <div style={user ? {margin: "40px 130px 0 130px"} : {visibility: "hidden"}}>
            <h2 style={{marginBottom: "40px"}}>
                <div style={{display: "inline-block", marginRight: "5px"}}>Пользователь:</div>
                <div style={{fontWeight: "bold", display: "inline-block"}}>{ user.user_name } </div>
            </h2>
            <ChangeUserName user = {user}/>
            <ChangeQuestions user = {user}/>
            <a style={{color: "grey", textDecoration: "none", fontSize: "10px"}}
               href="https://gitlab.com/aerdo/course-work-data-protection-ui"
               target="_blank">
                Информация о проекте
            </a>
        </div>
    )
}

export default Profile;