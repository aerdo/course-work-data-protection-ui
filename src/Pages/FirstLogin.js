import React from "react";
import QuestionsInput from "../Components/Login/QuestionsInput";

function FirstLogin(props){
    document.title = "Вход в систему";
    return(
        <div style={{margin: "0 130px 0 130px"}}>
            <h2>Это первый вход в систему. Придумайте вопросы и ответы на них</h2>
            <h4 style={{marginBottom: "40px"}}>Обращаем внимание на то, что вопросы и ответы могут нести любую смысловую нагрузку</h4>
            <QuestionsInput user ={props.user}/>
        </div>
    )
}

export default FirstLogin;