import React from "react";
import Admin from "../Components/Main/Admin/Admin";

function Administration(){
    document.title = "Администрирование";
    return(
        <div style={{margin: "40px 130px 0 130px"}}>
            <Admin/>
        </div>
    )
}
export default Administration;